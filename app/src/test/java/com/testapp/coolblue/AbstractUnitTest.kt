package com.testapp.coolblue

import okhttp3.mockwebserver.MockWebServer
import org.junit.After


abstract class AbstractUnitTest {

    val mockServer: MockWebServer = MockWebServer()

    @After
    open fun tearDown() {
        mockServer.shutdown()
    }
}