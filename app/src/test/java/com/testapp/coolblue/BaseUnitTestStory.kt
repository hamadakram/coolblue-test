package com.testapp.coolblue

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.testapp.coolblue.data.source.remote.ProductRemoteDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.SocketPolicy
import okio.Buffer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import javax.inject.Inject


/**
 * Created by Hammad Akram on 02/07/2020.
 * hammad.akram@accenture.com
 */

open class BaseUnitTestStory: AbstractUnitTest() {

    @get:Rule
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    private val mainThreadSurrogate: TestCoroutineDispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)

    }
    @ExperimentalCoroutinesApi
    @After
    fun onStop() {
        Dispatchers.resetMain()
        mainThreadSurrogate.cleanupTestCoroutines()
    }

    fun getResponseFromFile(statusCode: Int, fileName: String) = MockResponse().apply {
        setResponseCode(statusCode)
        val resourceAsStream = this.javaClass.getResourceAsStream("/jsonResponses/${fileName}")
        resourceAsStream?.let { setBody(Buffer().readFrom(it)) }
    }

    fun getResponse(statusCode: Int, body: String) = MockResponse().apply {
        setResponseCode(statusCode)
        setBody(body)
    }

    fun getTimeoutRequest(): MockResponse =
        MockResponse().apply { setSocketPolicy(SocketPolicy.NO_RESPONSE) }

    fun getConnectionRefusedRequest(): MockResponse =
        MockResponse().apply { setSocketPolicy(SocketPolicy.DISCONNECT_AT_START) }
}