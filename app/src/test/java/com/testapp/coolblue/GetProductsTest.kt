package com.testapp.coolblue

import com.testapp.coolblue.data.source.remote.ProductRemoteDataSource
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject


/**
 * Created by Hammad Akram on 02/07/2020.
 * hammad.akram@accenture.com
 */
@HiltAndroidTest
class GetProductsTest: BaseUnitTestStory() {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var remoteDataSource: ProductRemoteDataSource

    @Before
    fun init() {
        hiltRule.inject()
    }

    // TODO: Complete unit tests
    @Test
    fun `get products 200`(){
        mockServer.enqueue(getResponseFromFile(200, "getproducts.json"))
        runBlocking {
            remoteDataSource.getProducts("", 1)
        }
    }
}