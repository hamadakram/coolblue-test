package com.testapp.coolblue.data.mapper

import com.testapp.coolblue.data.model.*
import com.testapp.coolblue.domain.model.*


/**
 * Created by Hammad Akram on 01/07/2020.
 * hammad.akram@accenture.com
 */

fun List<NetworkProduct>.toDomain() = map { it.toDomain() }

fun NetworkProduct.toDomain() = Product(
    id = productId,
    name = productName,
    text = productText,
    reviewInformation = reviewInformation?.toDomain(),
    usps = usps,
    pros = pros,
    cons = cons,
    available = availabilityState == 2,
    price = salesPriceIncVat,
    mainImage = productImage,
    images = productImages,
    deliveredWith = deliveredWith,
    title = coolbluesChoiceInformationTitle,
    promoIcon = promoIcon?.toDomain(),
    specificationSummary = specificationSummary?.map { it.toDomain() },
    nextDayDelivery = nextDayDelivery,
    recommendedAccessories = recommendedAccessories
)

fun NetworkReviewInformation.toDomain() = ReviewInformation(
    reviews = reviews?.map { it.toDomain() },
    summary = reviewSummary?.toDomain()
)

fun NetworkReviewSummary.toDomain() = ReviewSummary(
    average = reviewAverage,
    count = reviewCount
)

fun NetworkPromoIcon.toDomain() = PromoIcon(
    text = text,
    type = type
)

fun NetworkSpecificationSummary.toDomain() = SpecificationSummary(
    name = name,
    value = value,
    stringValue = stringValue
)

fun NetworkReviews.toDomain() = Reviews(
    reviewId = reviewId,
    pros = pros,
    cons = cons,
    creationDate = creationDate,
    creatorName = creatorName,
    description = description,
    title = title,
    rating = rating,
    languageCode = languageCode
)