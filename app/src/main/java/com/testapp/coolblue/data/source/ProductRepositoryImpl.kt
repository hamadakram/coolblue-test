package com.testapp.coolblue.data.source

import com.testapp.coolblue.data.source.remote.ProductRemoteDataSource
import com.testapp.coolblue.domain.ProductRepository
import com.testapp.coolblue.domain.model.Product
import javax.inject.Inject

class ProductRepositoryImpl @Inject constructor(
    private val remoteDataSource: ProductRemoteDataSource
) : ProductRepository {

    override suspend fun getProducts(searchQuery: String, page: Int): List<Product> {
        return remoteDataSource.getProducts(searchQuery, page)
    }

    override suspend fun getProduct(id: Int): Product {
        return remoteDataSource.getProduct(id)
    }
}