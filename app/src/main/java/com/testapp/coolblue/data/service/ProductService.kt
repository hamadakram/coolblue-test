package com.testapp.coolblue.data.service

import com.testapp.coolblue.data.model.response.ProductListResponse
import com.testapp.coolblue.data.model.response.ProductResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ProductService {
    @GET("/mobile-assignment/search")
    suspend fun getProducts(
        @Query("query") query: String,
        @Query("page") page: Int
    ): ProductListResponse

    @GET("/mobile-assignment/product/{productId}")
    suspend fun getProduct(
        @Path("productId") productId: Int
    ): ProductResponse

}