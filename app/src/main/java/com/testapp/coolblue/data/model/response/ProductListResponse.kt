package com.testapp.coolblue.data.model.response

import com.testapp.coolblue.data.model.NetworkProduct

data class ProductListResponse(
    val products: List<NetworkProduct>,
    val currentPage: Int,
    val pageSize: Int,
    val totalResults: Int,
    val pageCount: Int
)