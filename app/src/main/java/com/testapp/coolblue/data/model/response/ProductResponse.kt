package com.testapp.coolblue.data.model.response

import com.testapp.coolblue.data.model.NetworkProduct

data class ProductResponse(
    val product: NetworkProduct
)