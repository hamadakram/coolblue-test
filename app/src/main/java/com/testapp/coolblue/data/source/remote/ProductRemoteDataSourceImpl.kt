package com.testapp.coolblue.data.source.remote

import com.testapp.coolblue.data.mapper.toDomain
import com.testapp.coolblue.data.service.ProductService
import com.testapp.coolblue.domain.model.Product
import javax.inject.Inject

class ProductRemoteDataSourceImpl @Inject constructor(
    private val productService: ProductService
) : ProductRemoteDataSource {

    override suspend fun getProducts(searchQuery: String, page: Int): List<Product> {
        return productService.getProducts(searchQuery, page).products.toDomain()
    }

    override suspend fun getProduct(id: Int): Product {
        return productService.getProduct(id).product.toDomain()
    }
}