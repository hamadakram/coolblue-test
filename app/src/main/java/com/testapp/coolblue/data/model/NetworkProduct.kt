package com.testapp.coolblue.data.model

import com.google.gson.annotations.SerializedName

data class NetworkProduct(
    val productId : Int,
    val productName : String?,
    val productText : String?,
    val reviewInformation : NetworkReviewInformation?,
    @SerializedName("USPs")
    val usps : List<String>?,
    val pros : List<String>?,
    val cons : List<String>?,
    val availabilityState : Int?,
    val salesPriceIncVat : Double,
    val productImage : String?,
    val productImages : List<String>,
    val deliveredWith: List<String>?,
    val coolbluesChoiceInformationTitle : String?,
    val promoIcon : NetworkPromoIcon?,
    val specificationSummary: List<NetworkSpecificationSummary>?,
    val nextDayDelivery : Boolean?,
    val recommendedAccessories: List<Int>?
)

data class NetworkPromoIcon (
    val text : String?,
    val type : String?
)

data class NetworkReviewInformation (
    val reviews : List<NetworkReviews>?,
    val reviewSummary : NetworkReviewSummary?
)

data class NetworkReviewSummary (
    val reviewAverage : Double?,
    val reviewCount : Int?
)

data class NetworkSpecificationSummary (
    val name : String?,
    val value : String?,
    val stringValue : String?
)

data class NetworkReviews (
    val reviewId : Int?,
    val pros : List<String>?,
    val cons : List<String>?,
    val creationDate : String?,
    val creatorName : String?,
    val description : String?,
    val title : String?,
    val rating : Double?,
    val languageCode : String?
)