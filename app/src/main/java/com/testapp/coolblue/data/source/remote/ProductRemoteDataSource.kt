package com.testapp.coolblue.data.source.remote

import com.testapp.coolblue.domain.model.Product

interface ProductRemoteDataSource {
    suspend fun getProducts(searchQuery: String, page: Int): List<Product>
    suspend fun getProduct(id: Int): Product
}