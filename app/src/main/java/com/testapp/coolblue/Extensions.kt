package com.testapp.coolblue

import androidx.appcompat.widget.SearchView

// Extension function to make clean
fun SearchView.onSearch(onSearchAction: (String) -> Unit) {
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextChange(newText: String): Boolean {
            return false
        }

        override fun onQueryTextSubmit(query: String): Boolean {
            onSearchAction.invoke(query)
            return true
        }
    })
}