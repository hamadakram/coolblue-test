package com.testapp.coolblue.ui.search

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.testapp.coolblue.domain.model.Product
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_product.view.*

class ProductViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    companion object {
        const val BULLET = "\u2022"
    }

    fun bind(product: Product, itemClickAction: ((item: Product) -> Unit)?) {
        with(product) {
            Glide.with(itemView.context).load(mainImage).into(itemView.ivThumbnail)
            itemView.tvProductName.text = product.name

            // Rating
            reviewInformation?.summary?.average?.toFloat()?.let {
                itemView.rbRatingBar.rating = it/2
            }
            // Rating summary
            reviewInformation?.summary?.count?.let {
                itemView.tvRatingSummary.text = it.toString()
            }
            // USPs
            usps?.joinToString("\n$BULLET  ")?.let {
                val uspsText = "$BULLET  $it"
                itemView.tvUsps.text = uspsText
            }
            // Price
            price?.let {
                itemView.tvPrice.text = it.toString()
            }
            // Next day delivery
            nextDayDelivery?.let {
                itemView.tvDeliver.visibility = if (it) View.VISIBLE else View.GONE
            }

            itemView.setOnClickListener { itemClickAction?.invoke(product) }

        }
    }
}