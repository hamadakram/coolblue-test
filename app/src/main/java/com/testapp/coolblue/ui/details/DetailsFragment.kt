package com.testapp.coolblue.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.testapp.coolblue.R
import com.testapp.coolblue.domain.model.Product
import com.testapp.coolblue.ui.ProductViewModel
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class DetailsFragment : Fragment() {

    companion object {
        private const val PRODUCT_KEY = "product_key"

        fun newInstance(product: Product): Fragment {
            return DetailsFragment().apply {
                val bundle = Bundle()
                bundle.putParcelable(PRODUCT_KEY, product)
                arguments = bundle
            }
        }
    }

    private val productViewModel: ProductViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let {
            toolbar.navigationIcon = ContextCompat.getDrawable(it, R.drawable.ic_back)
            toolbar.setNavigationOnClickListener {
                activity?.onBackPressed()
            }
        }

        arguments?.getParcelable<Product>(PRODUCT_KEY)?.let {
            setProduct(it)
            productViewModel.getProductDetails(it.id)
        }



        productViewModel.product.observe(viewLifecycleOwner, Observer {
            setProduct(it)
        })
    }

    private fun setProduct(product: Product) {
        with(product) {
            Glide.with(ivProductImage.context).load(images?.get(0)).into(ivProductImage)
            tvProductName.text = product.name

            // Rating
            reviewInformation?.summary?.average?.toFloat()?.let {
                rbRatingBar.rating = it / 2
            }
            // Rating summary
            reviewInformation?.summary?.count?.let {
                tvRatingSummary.text = it.toString()
            }
//            // USPs
//            usps?.joinToString("\n${ProductViewHolder.BULLET}  ")?.let {
//                val uspsText = "${ProductViewHolder.BULLET} $it"
//                tvUsps.text = uspsText
//            }
            // Price
            price?.let {
                tvPrice.text = it.toString()
            }
            // Next day delivery
            nextDayDelivery?.let {
                tvDeliver.visibility = if (it) View.VISIBLE else View.GONE
            }
        }
    }
}