package com.testapp.coolblue.ui.search

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.testapp.coolblue.R
import com.testapp.coolblue.SpacesItemDecoration
import com.testapp.coolblue.onSearch
import com.testapp.coolblue.ui.ParentActivity
import com.testapp.coolblue.ui.ProductViewModel
import com.testapp.coolblue.ui.details.DetailsFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.progress_layout.*

@AndroidEntryPoint
class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private val productViewModel: ProductViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpToolbar()
        setUpRecyclerView()

        // Product list observer
        productViewModel.products.observe(viewLifecycleOwner, Observer {
            (rvProducts.adapter as? SearchAdapter)?.submitList(it)
        })

        // Progress bar status observer
        productViewModel.progress.observe(viewLifecycleOwner, Observer { inProgress ->
            progressLayout.visibility = if (inProgress) View.VISIBLE else View.GONE
        })

        productViewModel.search("")
    }

    private fun setUpRecyclerView() {
        val adapter = SearchAdapter()
        rvProducts.adapter = adapter
        rvProducts.layoutManager = GridLayoutManager(context, 1)
        rvProducts.addItemDecoration(SpacesItemDecoration(50))

        adapter.itemClickAction = {
            (activity as? ParentActivity)?.addFragment(DetailsFragment.newInstance(it), true)
        }
    }

    private fun setUpToolbar() {
        toolbar.inflateMenu(R.menu.search_menu)
        (toolbar.menu.findItem(R.id.action_search).actionView as? SearchView)?.let { searchView ->
            searchView.onSearch { productViewModel.search(it) }
        }
    }
}