package com.testapp.coolblue.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testapp.coolblue.domain.ProductRepository
import com.testapp.coolblue.domain.model.Product
import kotlinx.coroutines.launch

class ProductViewModel @ViewModelInject constructor(private val productRepository: ProductRepository) :
    ViewModel() {

    companion object {
        const val PAGE_START = 1
    }

    private var searchQuery = ""
    private var page = PAGE_START

    private val _products = MutableLiveData<List<Product>>()
    val products: LiveData<List<Product>>
        get() = _products

    private val _product = MutableLiveData<Product>()
    val product: LiveData<Product>
        get() = _product

    private val _progress = MutableLiveData<Boolean>()
    val progress: LiveData<Boolean>
        get() = _progress

    fun search(searchQuery: String) {
        this.searchQuery = searchQuery
        this.page = PAGE_START
        getProducts()
    }

    fun getProducts() {
        _progress.value = true
        viewModelScope.launch {
            val productList = productRepository.getProducts(searchQuery, page)
            _products.value = productList
            _progress.value = false
        }
    }

    fun getProductDetails(id: Int){
        _progress.value = true
        viewModelScope.launch {
            val product = productRepository.getProduct(id)
            _product.value = product
            _progress.value = false
        }
    }
}