package com.testapp.coolblue

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoolBlueApplication: Application() {
}