package com.testapp.coolblue.di.module

import com.testapp.coolblue.data.source.ProductRepositoryImpl
import com.testapp.coolblue.data.source.remote.ProductRemoteDataSource
import com.testapp.coolblue.data.source.remote.ProductRemoteDataSourceImpl
import com.testapp.coolblue.domain.ProductRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@InstallIn(ActivityComponent::class)
@Module
abstract class ProductModule {

    @Binds
    abstract fun bindProductRepository(productRepositoryImpl: ProductRepositoryImpl): ProductRepository

    @Binds
    abstract fun bindProductRemoteDataSource(productRemoteDataSourceImpl: ProductRemoteDataSourceImpl): ProductRemoteDataSource

}