package com.testapp.coolblue.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    val id: Int,
    val name: String?,
    val text: String?,
    val reviewInformation: ReviewInformation?,
    val usps: List<String>?,
    val pros: List<String>?,
    val cons: List<String>?,
    val available: Boolean?,
    val price: Double?,
    val mainImage: String?,
    val images: List<String>?,
    val deliveredWith: List<String>?,
    val title: String?,
    val promoIcon: PromoIcon?,
    val specificationSummary: List<SpecificationSummary>?,
    val nextDayDelivery: Boolean?,
    val recommendedAccessories: List<Int>?
) : Parcelable

@Parcelize
data class PromoIcon(
    val text: String?,
    val type: String?
) : Parcelable

@Parcelize
data class ReviewInformation(
    val reviews: List<Reviews>?,
    val summary: ReviewSummary?
) : Parcelable

@Parcelize
data class ReviewSummary(
    val average: Double?,
    val count: Int?
) : Parcelable

@Parcelize
data class SpecificationSummary(
    val name: String?,
    val value: String?,
    val stringValue: String?
) : Parcelable

@Parcelize
data class Reviews(
    val reviewId: Int?,
    val pros: List<String>?,
    val cons: List<String>?,
    val creationDate: String?,
    val creatorName: String?,
    val description: String?,
    val title: String?,
    val rating: Double?,
    val languageCode: String?
) : Parcelable