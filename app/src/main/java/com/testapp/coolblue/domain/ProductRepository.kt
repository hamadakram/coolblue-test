package com.testapp.coolblue.domain

import com.testapp.coolblue.domain.model.Product

interface ProductRepository {
    suspend fun getProducts(searchQuery: String, page: Int): List<Product>
    suspend fun getProduct(id: Int): Product
}