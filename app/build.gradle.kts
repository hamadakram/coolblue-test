plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdkVersion(Apps.compileSdk)
    buildToolsVersion = "30.0.0"

    defaultConfig {
        applicationId = "com.testapp.coolblue"
        minSdkVersion(Apps.minSdk)
        targetSdkVersion(Apps.targetSdk)
        versionCode = Apps.versionCode
        versionName = Apps.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
            )

        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(Libs.kotlin)
    implementation(Libs.AndroidX.appcompat)
    implementation(Libs.AndroidX.core)
    implementation(Libs.AndroidX.fragmentKtx)
    implementation(Libs.AndroidX.constraintLayout)
    implementation(Libs.AndroidX.lifeCycle)
    implementation(Libs.AndroidX.recyclerView)

    // Gson
    implementation(Libs.gson)

    // Glide
    implementation(Libs.glide)
    annotationProcessor(Libs.glideCompiler)

    // Retrofit
    implementation(Libs.retrofit)

    // Hilt
    implementation(Libs.hilt)
    kapt(Libs.hiltCompiler)

    // Coroutines
    implementation(Libs.coroutines)

    // Test
    testImplementation(Libs.testDependencies)

    testImplementation("com.google.dagger:hilt-android-testing:2.28-alpha")
    kaptTest("com.google.dagger:hilt-android-compiler:2.28-alpha")

    androidTestImplementation("com.google.dagger:hilt-android-testing:2.28-alpha")
    // ...with Kotlin.
    kaptAndroidTest ("com.google.dagger:hilt-android-compiler:2.28-alpha")
    // ...with Java.
    androidTestAnnotationProcessor ("com.google.dagger:hilt-android-compiler:2.28-alpha")

}