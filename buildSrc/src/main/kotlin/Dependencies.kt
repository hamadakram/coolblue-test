import org.gradle.api.artifacts.dsl.DependencyHandler

object Apps {
    const val compileSdk = 29
    const val minSdk = 21
    const val targetSdk = 29
    const val versionCode = 1
    const val versionName = "1.0.0"
}

object Versions {
    const val gradle = "4.0.0"
    const val kotlin = "1.3.50"
    const val appcompat = "1.1.0"
    const val coreKtx = "1.3.0"
    const val fragmentKtx = "1.2.5"
    const val paging = "2.1.0"
    const val constraintsLayout = "1.1.3"
    const val lifecycle = "2.3.0-alpha05"
    const val recyclerView = "1.0.0"
    const val rxKotlin = "2.4.0"
    const val rxAndroid = "2.1.1"
    const val retrofit = "2.6.2"
    const val httpLogging = "4.7.2"
    const val dagger = "2.25.2"
    const val daggerAndroid = "2.24"
    const val gson = "2.8.5"
    const val glide = "4.11.0"
    const val coroutines = "1.3.2"
    const val hilt = "2.28-alpha"
    const val hiltLifeCycle = "1.0.0-alpha01"
}

object Libs {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"

    val testDependencies = arrayOf(
        "junit:junit:4.12",
        "org.mockito:mockito-core:3.3.3",
        "androidx.arch.core:core-testing:2.1.0",
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.3.7",
        "com.squareup.okhttp3:mockwebserver:4.7.2"
    )

    object AndroidX {
        const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
        const val core = "androidx.core:core-ktx:${Versions.coreKtx}"
        const val constraintLayout =
            "androidx.constraintlayout:constraintlayout:${Versions.constraintsLayout}"
        const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
        const val fragmentKtx = "androidx.fragment:fragment-ktx:${Versions.fragmentKtx}"
        const val paging = "androidx.paging:paging-runtime${Versions.paging}"

        val lifeCycle = arrayOf(
            "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}",
            "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle}",
            "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
        )
    }

    val coroutines = arrayOf(
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}",
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    )

    val rxJava = arrayOf(
        "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin}",
        "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    )

    val retrofit = arrayOf(
        "com.squareup.retrofit2:retrofit:${Versions.retrofit}",
        "com.squareup.retrofit2:converter-gson:${Versions.retrofit}",
        "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}",
        "com.squareup.okhttp3:logging-interceptor:${Versions.httpLogging}"
    )

    val dagger = arrayOf(
        "com.google.dagger:dagger:${Versions.dagger}",
        "com.google.dagger:dagger-android:${Versions.daggerAndroid}",
        "com.google.dagger:dagger-android-support:${Versions.daggerAndroid}"
    )

    val daggerCompiler = arrayOf(
        "com.google.dagger:dagger-android-processor:${Versions.daggerAndroid}",
        "com.google.dagger:dagger-compiler:${Versions.dagger}"
    )

    val hilt = arrayOf(
        "com.google.dagger:hilt-android:${Versions.hilt}",
        "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltLifeCycle}"
    )

    val hiltCompiler = arrayOf(
        "com.google.dagger:hilt-android-compiler:${Versions.hilt}",
        "androidx.hilt:hilt-compiler:${Versions.hiltLifeCycle}"
    )

}

/**
 * DependencyHandler extensions
 */

fun DependencyHandler.implementation(dependencyNotations: Array<String>) {
    dependencyNotations.forEach { add("implementation", it) }
}

fun DependencyHandler.kapt(dependencyNotations: Array<String>) {
    dependencyNotations.forEach { add("kapt", it) }
}

fun DependencyHandler.testImplementation(dependencyNotations: Array<String>) {
    dependencyNotations.forEach { add("testImplementation", it) }
}